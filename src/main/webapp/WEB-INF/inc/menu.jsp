<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="menu">
    <p><a href="<c:url value="/createCustomer"/>">Créer un nouveau client</a></p>
    <p><a href="<c:url value="/createOrder"/>">Créer une nouvelle commande</a></p>
    <p><a href="<c:url value="/createUser"/>">Créer un nouvel utilisateur</a></p>
    <p><a href="<c:url value="/listCustomers"/>">Voir les clients existants</a></p>
    <p><a href="<c:url value="/listOrders"/>">Voir les commandes existantes</a></p>
    <p><a href="<c:url value="/listUsers"/>">Voir les utilisateurs existants</a></p>
</div>