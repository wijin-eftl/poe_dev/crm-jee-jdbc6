<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<label for="lastname">Nom <span class="required">*</span></label>
<input type="text" id="lastname" name="lastname" value="<c:out value="${customer.lastname}"/>" size="50"
	maxlength="100" />
<span class="error">${form.errors['lastname']}</span>
<br />

<label for="firstname">Prénom </label>
<input type="text" id="firstname" name="firstname" value="<c:out value="${customer.firstname}"/>" size="50"
	maxlength="100" />
<span class="error">${form.errors['firstname']}</span>
<br />

<label for="company">Entreprise <span class="required">*</span></label>
<input type="text" id="company" name="company" value="<c:out value="${customer.company}"/>" size="50"
	maxlength="200" />
<span class="error">${form.errors['company']}</span>
<br />

<label for="phone">Téléphone <span class="required">*</span></label>
<input type="text" id="phone" name="phone" value="<c:out value="${customer.phone}"/>" size="20"
	maxlength="15" />
<span class="error">${form.errors['phone']}</span>
<br />

<label for="mobile">Mobile</label>
<input type="text" id="mobile" name="mobile" value="" size="20"
	maxlength="15" />
<br />

<label for="mail">Adresse email <span class="required">*</span></label>
<input type="email" id="mail" name="mail" value="<c:out value="${customer.mail}"/>" size="50"
	maxlength="255" />
<span class="error">${form.errors['mail']}</span>
<br />

<label for="notes">Notes</label>
<textarea id="notes" name="notes" rows="4" cols="60" maxlength="255"><c:out value="${customer.notes}"/></textarea>
<span class="error">${form.errors['notes']}</span>
<br />