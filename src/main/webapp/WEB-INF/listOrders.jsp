<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Liste des commandes existantes</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="/inc/style.css"/>" />
    </head>
    <body>
        <c:import url="/WEB-INF/inc/menu.jsp" />
        <div id="corps">
        <c:choose>
            <%-- Si aucune commande n'existe en session, affichage d'un message par défaut --%>
            <c:when test="${empty sessionScope.orders}">
                <p class="error">Aucune commande enregistrée</p>
            </c:when>
            <%-- Sinon, affichage du tableau. --%>
            <c:otherwise>
            <table>
                <tr>
                    <th>Client</th>
                    <th>Libellé</th>
                    <th>Nombre de jours</th>
                    <th>TJM HT</th>
                    <th>TVA</th>
                    <th>Type</th>
                    <th>Statut</th>
                    <th class="action">Action</th>                    
                </tr>
                <%-- Parcours de la Map des commandes en session, et utilisation de l'objet varStatus --%>
                <c:forEach items="${sessionScope.orders}" var="ordersMap" varStatus="boucle">
                <%-- Simple test de parité sur l'index de parcours, pour alterner la couleur de fond de chaque ligne du tableau --%>
                <tr class="${boucle.index % 2 == 0 ? 'pair' : 'impair'}">
                    <%-- Affichage des propriétés du bean Order, qui est stocké en tant que valeur de l'entrée courante de la map --%>
                    <td><c:out value="${ordersMap.value.customer.firstname} ${ordersMap.value.customer.lastname}"/></td>
                    <td><c:out value="${ordersMap.value.label}"/></td>
                    <td><c:out value="${ordersMap.value.numberOfDays}"/></td>
                    <td><c:out value="${ordersMap.value.adrEt}"/></td>
                    <td><c:out value="${ordersMap.value.tva}"/></td>
                    <td><c:out value="${ordersMap.value.type}"/></td>
                    <td><c:out value="${ordersMap.value.status}"/></td>
                    <%-- Lien vers la servlet de suppression, avec passage de l'id de la commande en paramètre grâce à la balise <c:param/> --%>
                    <td class="action">
                        <a href="<c:url value="/deleteOrder"><c:param name="orderId" value="${ordersMap.key}" /></c:url>">
                            <img src="<c:url value="/inc/delete.png"/>" alt="Supprimer" />
                        </a>
                    </td>
                </tr>
                </c:forEach>
            </table>
            </c:otherwise>
        </c:choose>
        </div>
    </body>
</html>