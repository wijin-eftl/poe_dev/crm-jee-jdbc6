<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Affichage d'un utilisateur</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="/inc/style.css"/>" />
    </head>
    <body>
    	<c:import url="/WEB-INF/inc/menu.jsp" />
    	<div id="corps">
    		<p class="info">${form.result}</p>
    		<p>Username : <c:out value="${user.username}"/></p>
		    <p>Mot de passe : <c:out value="${user.password}"/></p>
		    <p>Email : <c:out value="${user.mail}"/></p>
		    <p>Droits : <c:out value="${user.grants}"/></p>
	    </div>
    </body>
</html>

