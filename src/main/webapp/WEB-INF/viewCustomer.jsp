<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Affichage d'un client</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="/inc/style.css"/>" />
    </head>
    <body>
    	<c:import url="/WEB-INF/inc/menu.jsp" />
        <div id="corps">
	        <p class="info">${form.result}</p>
		    <p>Nom : <c:out value="${customer.lastname}"/></p>
		    <p>Prénom : <c:out value="${customer.firstname}"/></p>
		    <p>Entreprise : <c:out value="${customer.company}"/></p>
		    <p>Téléphone : <c:out value="${customer.phone}"/></p>
		    <p>Mobile : <c:out value="${customer.mobile}"/></p>
		    <p>Email : <c:out value="${customer.mail}"/></p>
		    <p>Notes : <c:out value="${customer.notes}"/></p>
        </div>
    </body>
</html>