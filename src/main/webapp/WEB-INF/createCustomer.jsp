<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Création d'un client</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="/inc/style.css"/>" />
    </head>
    <body>
    	<c:import url="/WEB-INF/inc/menu.jsp" />
        <div>
            <form method="post" action="<c:url value="/createCustomer"/>">
                <fieldset>
                    <legend>Informations client</legend>
                    <c:import url="/WEB-INF/inc/inc_customer_form.jsp" />
                </fieldset>
                <p class="info">${form.result}</p>
                <input class="submitButton" type="submit" value="Valider"/>
                <input class="resetButton" type="reset" value="Remettre à zéro"/> <br/>
            </form>
        </div>
    </body>
</html>