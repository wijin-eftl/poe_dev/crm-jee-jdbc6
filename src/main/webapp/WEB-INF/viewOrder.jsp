<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Affichage d'une commande</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="/inc/style.css"/>" />
    </head>
    <body>
    	<c:import url="/WEB-INF/inc/menu.jsp" />
    	<div id="corps">
	        <p class="info">${form.result}</p>
		    <p>Client</p>
		    <p>Nom : <c:out value="${order.customer.lastname}"/></p>
		    <p>Prénom : <c:out value="${order.customer.firstname}"/></p>
		    <p>Entreprise : <c:out value="${order.customer.company}"/></p>
		    <p>Téléphone : <c:out value="${order.customer.phone}"/></p>
		    <p>Mobile : <c:out value="${order.customer.mobile}"/></p>
		    <p>Email : <c:out value="${order.customer.mail}"/></p>
		    <p>Notes : <c:out value="${order.customer.notes}"/></p>
		    <p>Commande</p>
		    <p>Libellé : <c:out value="${order.label}"/></p>
		    <p>Nombre de jours : <c:out value="${order.numberOfDays}"/></p>
		    <p>TJM HT : <c:out value="${order.adrEt}"/></p>
		    <p>TVA : <c:out value="${order.tva}"/></p>
		    <p>Type de commande : <c:out value="${order.type}"/></p>
		    <p>Statut : <c:out value="${order.status}"/></p>
		    <p>Notes : <c:out value="${order.notes}"/></p>
        </div>
    </body>
</html>