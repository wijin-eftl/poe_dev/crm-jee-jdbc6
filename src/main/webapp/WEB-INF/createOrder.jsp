<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Création d'une commande</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="/inc/style.css"/>" />
    </head>
    <body>
    	<c:import url="/WEB-INF/inc/menu.jsp" />
        <div>
            <form method="post" action="<c:url value="/createOrder"/>">
                <fieldset>
                    <legend>Informations client</legend>
                    <%-- Si et seulement si la Map des clients en session n'est pas vide, alors on propose un choix à l'utilisateur --%>
                    <c:if test="${!empty sessionScope.customers}">
                        <label for="customerChoice">Nouveau client ? <span class="requis">*</span></label>
                        <input type="radio" id="customerChoice" name="customerChoice" value="newCustomer" checked /> Oui
                        <input type="radio" id="customerChoice" name="customerChoice" value="oldCustomer" /> Non
                        <br/><br />
                    </c:if>
                    
                    <c:set var="customer" value="${order.customer}" scope="request" />
                    <div id="newCustomer">
                        <c:import url="/WEB-INF/inc/inc_customer_form.jsp" />
                    </div>
                    
                    <%-- Si et seulement si la Map des clients en session n'est pas vide, alors on crée la liste déroulante --%>
                    <c:if test="${!empty sessionScope.customers}">
                    <div id="oldCustomer">
                        <select name="customersList" id="customersList">
                            <option value="">Choisir un client : </option>
                            <%-- Boucle sur la map des clients --%>
                            <c:forEach items="${sessionScope.customers}" var="customersMap">
                            <%--  L'expression EL ${customersMap.value} permet de sélectionner l'objet Customer stocké en tant que valeur dans la Map --%>
                            <option value="${customersMap.key}">${customersMap.value.firstname} ${customersMap.value.lastname}</option>
                            </c:forEach>
                        </select>
                    </div>
                    </c:if>
                </fieldset>
                <fieldset>
                    <legend>Informations commande</legend>
                    
                    <label for="label">Libellé <span class="required">*</span></label>
                    <input type="text" id="label" name="label" value="<c:out value="${order.label}"/>" size="100" maxlength="100"/>
                    <span class="error">${form.errors['label']}</span>
                    <br/>
                    
                    <label for="numberOfDays">Nombre de jours <span class="required">*</span></label>
                    <input type="number" id="numberOfDays" name="numberOfDays" value="<c:out value="${order.numberOfDays}"/>" size="2" step=0.01 />
                    <span class="error">${form.errors['numberOfDays']}</span>
                    <br/>
                    
                    <label for="adrEt">TJM HT <span class="required">*</span></label>
                    <input type="number" id="adrEt" name="adrEt" value="<c:out value="${order.adrEt}"/>" size="2" maxlength="2" step=0.01 />
                    <span class="error">${form.errors['adrEt']}</span>
                    <br/>
                    
                    <label for="tva">Taux TVA <span class="required">*</span></label>
                    <input type="number" id="tva" name="tva" value="<c:out value="${order.tva}"/>" size="2" maxlength="2" step=0.01 />
                    <span class="error">${form.errors['tva']}</span>
                    <br/>
                    
                    <label for="type">Type de commande <span class="required">*</span></label>
                    <input type="text" id="type" name="type" value="<c:out value="${order.type}"/>" size="30" maxlength="100" />
                    <span class="error">${form.errors['type']}</span>
                    <br/>
                    
                    <label for="status">Statut <span class="required">*</span></label>
                    <input type="text" id="status" name="status" value="<c:out value="${order.status}"/>" size="30" maxlength="30" />
                    <span class="error">${form.errors['status']}</span>
                    <br/>
                    
                    <label for="orderNotes">Notes</label>
                    <textarea id="orderNotes" name="orderNotes" rows="4" cols="60" maxlength="255"><c:out value="${order.notes}"/></textarea>
                    <span class="error">${form.errors['notes']}</span>
                    <br/>
                </fieldset>
                <p class="info">${form.result}</p>
                <input class="submitButton" type="submit" value="Valider" />
                <input class="resetButton" type="reset" value="Remettre à zéro"/> <br/>
            </form>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script> 
        <%-- Fonction jQuery permettant le remplacement de la première partie du formulaire par la liste déroulante au clic sur le bouton radio. --%>
        <script>
        	jQuery(document).ready(function(){
        		/* 1 - Au lancement de la page, on cache le bloc d'éléments du formulaire correspondant aux clients existants */
        		$("div#oldCustomer").hide();
        		/* 2 - Au clic sur un des deux boutons radio "customerChoice", on affiche le bloc d'éléments correspondant (nouveau ou ancien client) */
                jQuery('input[name=customerChoice]:radio').click(function(){
                	$("div#newCustomer").hide();
                	$("div#oldCustomer").hide();
                    var divId = jQuery(this).val();
                    $("div#"+divId).show();
                });
            });
        </script>
    </body>
</html>