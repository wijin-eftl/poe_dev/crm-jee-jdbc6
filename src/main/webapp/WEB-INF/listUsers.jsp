<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Liste des utilisateurs existants</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="/inc/style.css"/>" />
    </head>
    <body>
        <c:import url="/WEB-INF/inc/menu.jsp" />
        <div id="corps">
        <c:choose>
            <%-- Si aucun utilisateur n'existe en session, affichage d'un message par défaut --%>
            <c:when test="${empty sessionScope.users}">
                <p class="error">Aucun utilisateur enregistré.</p>
            </c:when>
            <%-- Sinon, affichage du tableau. --%>
            <c:otherwise>
            <table>
                <tr>
                    <th>Username</th>
                    <th>Mot de passe</th>
                    <th>Email</th>
                    <th>Droits</th>
                    <th class="action">Action</th>                    
                </tr>
                <%-- Parcours de la Map des utilisateurs en session, et utilisation de l'objet varStatus --%>
                <c:forEach items="${sessionScope.users}" var="usersMap" varStatus="boucle">
                <%-- Simple test de parité sur l'index de parcours, pour alterner la couleur de fond de chaque ligne du tableau. --%>
                <tr class="${boucle.index % 2 == 0 ? 'pair' : 'impair'}">
                    <%-- Affichage des propriétés du bean User, qui est stocké en tant que valeur de l'entrée courante de la map --%>
                    <td><c:out value="${usersMap.value.username}"/></td>
                    <td><c:out value="${usersMap.value.password}"/></td>
                    <td><c:out value="${usersMap.value.mail}"/></td>
                    <td><c:out value="${usersMap.value.grants}"/></td>
                    <%-- Lien vers la servlet de suppression, avec passage de l'id de l'utilisateur en paramètre grâce à la balise <c:param/> --%>
                    <td class="action">
                        <a href="<c:url value="/deleteUser"><c:param name="userId" value="${usersMap.key}" /></c:url>">
                            <img src="<c:url value="/inc/delete.png"/>" alt="Supprimer" />
                        </a>
                    </td>
                </tr>
                </c:forEach>
            </table>
            </c:otherwise>
        </c:choose>
        </div>
    </body>
</html>