<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Création d'un utilisateur</title>
        <link type="text/css" rel="stylesheet" href="inc/style.css" />
    </head>
    <body>
        <div>
        	<c:import url="/WEB-INF/inc/menu.jsp" />
            <form method="post" action="<c:url value="/createUser"/>">
                <fieldset>
                    <legend>Informations utilisateur</legend>
    
                    <label for="username">Username <span class="required">*</span></label>
                    <input type="text" id="username" name="username" value="<c:out value="${user.username}"/>" size="50" maxlength="100"/>
                    <span class="error">${form.errors['username']}</span>
                    <br/>
                    
                    <label for="password">Mot de passe <span class="required">*</span></label>
                    <input type="password" id="password" name="password" value="<c:out value="${user.password}"/>" size="50" maxlength="100"/>
                    <span class="error">${form.errors['password']}</span>
                    <br/>

                    <label for="mail" >Adresse email <span class="required">*</span></label>
                    <input type="email" id="mail" name="mail" value="<c:out value="${user.mail}"/>" size="50" maxlength="100"/>
                    <span class="error">${form.errors['mail']}</span>
                    <br/>

                    <label for="grants">Droits </label>
                    <input type="text" id="grants" name="grants" value="<c:out value="${user.grants}"/>" size="50" maxlength="100"/>
                    <span class="error">${form.errors['grants']}</span>
                    <br/>
                </fieldset>
                <p class="info">${form.result}</p>
                <input class="submitButton" type="submit" value="Valider"/>
                <input class="resetButton" type="reset" value="Remettre à zéro"/> <br/>
            </form>
        </div>
    </body>
</html>
