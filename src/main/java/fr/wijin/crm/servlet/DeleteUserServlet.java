package fr.wijin.crm.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fr.wijin.crm.dao.UserDao;
import fr.wijin.crm.dao.impl.DaoFactory;
import fr.wijin.crm.exception.DaoException;
import fr.wijin.crm.form.FormUtils;
import fr.wijin.crm.model.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/deleteUser")
public class DeleteUserServlet extends HttpServlet {

	private static final long serialVersionUID = 7599144677112007181L;

	public static final String SESSION_USERS = "users";
	
	private UserDao userDao;
	
	@Override
	public void init() throws ServletException {
        this.userDao = ((DaoFactory) getServletContext().getAttribute("daoFactory")).getUserDao();
    }

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String userId = FormUtils.getParameterValue(request, "userId");
		Integer id = Integer.parseInt(userId);

		// Récupération de la Map des clients enregistrés en session 
		HttpSession session = request.getSession();
		Map<Integer, User> users = (HashMap<Integer, User>) session.getAttribute(SESSION_USERS);

		if (id != null && users != null) {
			try {
				this.userDao.deleteUser(users.get(id));
				// Suppression de l'utilisateur de la Map
				users.remove(id);
			} catch (DaoException e) {
				e.printStackTrace();
				// Par exemple : afficher un message à l'utilisateur
			}
			// Repositionnement de la Map en session
			session.setAttribute(SESSION_USERS, users);
		}

		response.sendRedirect(request.getContextPath() + "/listUsers");
	}

}
