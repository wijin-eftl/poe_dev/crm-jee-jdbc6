package fr.wijin.crm.servlet;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/listCustomers")
public class ListCustomersServlet extends HttpServlet {

	private static final long serialVersionUID = 4746895303891018590L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Affichage de la liste des clients
		this.getServletContext().getRequestDispatcher("/WEB-INF/listCustomers.jsp").forward(request, response);
	}
}
