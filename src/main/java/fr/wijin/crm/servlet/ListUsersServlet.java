package fr.wijin.crm.servlet;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/listUsers")
public class ListUsersServlet extends HttpServlet {

	private static final long serialVersionUID = -6438804924379020955L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Affichage de la liste des utilisateurs
		this.getServletContext().getRequestDispatcher("/WEB-INF/listUsers.jsp").forward(request, response);
	}
}
