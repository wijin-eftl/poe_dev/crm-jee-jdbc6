package fr.wijin.crm.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fr.wijin.crm.dao.UserDao;
import fr.wijin.crm.dao.impl.DaoFactory;
import fr.wijin.crm.form.CreateUserForm;
import fr.wijin.crm.model.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/createUser")
public class UserServlet extends HttpServlet {
	
	private static final long serialVersionUID = -3963097623661296134L;

	private static final String USERS_SESSION_ATTRIBUTE = "users";
	
	private UserDao userDao;
	
	@Override
	public void init() throws ServletException {
        this.userDao = ((DaoFactory) getServletContext().getAttribute("daoFactory")).getUserDao();
    }

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/createUser.jsp").forward(request, response);	
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CreateUserForm form = new CreateUserForm(this.userDao);

        User user = form.createUser(request);

		request.setAttribute("user", user);
		request.setAttribute("form", form );
		
		if (form.getErrors().isEmpty()) {
			// Pas d'erreur : récupération de la Map des utilisateurs dans la session
            HttpSession session = request.getSession();
            Map<Integer, User> users = (HashMap<Integer, User>) session.getAttribute(USERS_SESSION_ATTRIBUTE);
            
            if (users == null) {
            	// Initialisation d'une Map si rien en session
            	users = new HashMap<>();
            }
            
            // Ajout du user courant dans la Map
            users.put(user.getId(), user);
            // Repositionnement de la Map en session
            session.setAttribute(USERS_SESSION_ATTRIBUTE, users);
            
            this.getServletContext().getRequestDispatcher("/WEB-INF/viewUser.jsp").forward(request, response);
        } else {
            this.getServletContext().getRequestDispatcher("/WEB-INF/createUser.jsp").forward(request, response);
        }
	}

}
