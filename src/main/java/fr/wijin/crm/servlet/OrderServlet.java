package fr.wijin.crm.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import fr.wijin.crm.dao.CustomerDao;
import fr.wijin.crm.dao.OrderDao;
import fr.wijin.crm.dao.impl.DaoFactory;
import fr.wijin.crm.form.CreateOrderForm;
import fr.wijin.crm.model.Customer;
import fr.wijin.crm.model.Order;

@WebServlet("/createOrder")
public class OrderServlet extends HttpServlet {

	private static final long serialVersionUID = 4626507962264184116L;
	
	private CustomerDao customerDao;
	private OrderDao orderDao;
	
	@Override
	public void init() throws ServletException {
		DaoFactory daoFactory = (DaoFactory) getServletContext().getAttribute("daoFactory");
        this.customerDao = daoFactory.getCustomerDao();
        this.orderDao = daoFactory.getOrderDao();
    }

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher("/WEB-INF/createOrder.jsp").forward(request, response);
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
        CreateOrderForm form = new CreateOrderForm(customerDao, orderDao);

        Order order = form.createOrder(request);

        request.setAttribute("order", order);
        request.setAttribute("form", form );
        
        /* Si aucune erreur */
        if (form.getErrors().isEmpty()) {
        	// Pas d'erreur : récupération de la Map des clients dans la session
            HttpSession session = request.getSession();
            Map<Integer, Customer> customers = (HashMap<Integer, Customer>) session.getAttribute("customers");

            if (customers == null) {
            	// Initialisation d'une Map si rien en session
                customers = new HashMap<>();
            }

            // Ajout du client courant dans la Map
            customers.put(order.getCustomer().getId(), order.getCustomer());
            // Repositionnement de la Map en session
            session.setAttribute("customers", customers);
 
            // Récupération de la Map des commandes dans la session
            Map<Integer, Order> orders = (HashMap<Integer, Order>) session.getAttribute("orders");
            
            if ( orders == null ) {
            	// Initialisation d'une Map si rien en session
                orders = new HashMap<>();
            }
            
            // Ajout de la commande courante dans la Map
            orders.put(order.getId(), order);
            // Repositionnement de la Map en session
            session.setAttribute("orders", orders);

            this.getServletContext().getRequestDispatcher("/WEB-INF/viewOrder.jsp").forward( request, response );
        } else {
        	// Affichage du formulaire avec les erreurs
            this.getServletContext().getRequestDispatcher("/WEB-INF/createOrder.jsp").forward( request, response );
        }
    }

}
