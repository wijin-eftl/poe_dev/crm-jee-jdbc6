package fr.wijin.crm.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import fr.wijin.crm.dao.CustomerDao;
import fr.wijin.crm.dao.impl.DaoFactory;
import fr.wijin.crm.form.CreateCustomerForm;
import fr.wijin.crm.model.Customer;

@WebServlet("/createCustomer")
public class CustomerServlet extends HttpServlet {
	
	private static final long serialVersionUID = 7721867609787488629L;

	private static final String CUSTOMERS_SESSION_ATTRIBUTE = "customers";
	
	private CustomerDao customerDao;
	
	@Override
	public void init() throws ServletException {
		// Récupération de la DaoFactory dans le ServletContext
        this.customerDao = ((DaoFactory) getServletContext().getAttribute("daoFactory")).getCustomerDao();
    }

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/createCustomer.jsp").forward( request, response );
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        CreateCustomerForm form = new CreateCustomerForm(this.customerDao);

        // Traitement de la requête et récupération du bean Customer
        Customer customer = form.createCustomer(request);

        // Ajout du bean Customer et de l'objet métier form à la requête
        request.setAttribute("customer", customer);
        request.setAttribute("form", form);
        
        if ( form.getErrors().isEmpty() ) {
            // Pas d'erreur : récupération de la Map des clients dans la session
            HttpSession session = request.getSession();
            Map<Integer, Customer> customers = (HashMap<Integer, Customer>) session.getAttribute(CUSTOMERS_SESSION_ATTRIBUTE);
            
            if (customers == null) {
            	// Initialisation d'une Map si rien en session
                customers = new HashMap<>();
            }
            // Ajout du client courant dans la Map
            customers.put(customer.getId(), customer);
            // Repositionnement de la Map en session
            session.setAttribute(CUSTOMERS_SESSION_ATTRIBUTE, customers);

            this.getServletContext().getRequestDispatcher("/WEB-INF/viewCustomer.jsp").forward( request, response );
        } else {
            // Affichage du formulaire avec les erreurs
            this.getServletContext().getRequestDispatcher("/WEB-INF/createCustomer.jsp").forward( request, response );
        }
    }

}
