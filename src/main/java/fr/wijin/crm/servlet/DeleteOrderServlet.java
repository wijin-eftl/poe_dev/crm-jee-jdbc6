package fr.wijin.crm.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import fr.wijin.crm.dao.OrderDao;
import fr.wijin.crm.dao.impl.DaoFactory;
import fr.wijin.crm.exception.DaoException;
import fr.wijin.crm.form.FormUtils;
import fr.wijin.crm.model.Order;

@WebServlet("/deleteOrder")
public class DeleteOrderServlet extends HttpServlet {

	private static final long serialVersionUID = -4595691347935781501L;
	
	public static final String PARAM_ID = "orderId";
	public static final String PARAM_LABEL = "label";
	public static final String SESSION_ORDERS = "orders";
	
	private OrderDao orderDao;
	
	@Override
	public void init() throws ServletException {
        this.orderDao = ((DaoFactory) getServletContext().getAttribute("daoFactory")).getOrderDao();
    }

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String orderId = FormUtils.getParameterValue(request, PARAM_ID);
		Integer id = Integer.parseInt(orderId);

		// Récupération de la Map des commandes enregistrées en session 
		HttpSession session = request.getSession();
		Map<Integer, Order> orders = (HashMap<Integer, Order>) session.getAttribute(SESSION_ORDERS);

		if (id != null && orders != null) {
			try {
				orderDao.deleteOrder(orders.get(id));
				// Suppression de la commande de la Map
				orders.remove(id);
			} catch (DaoException e) {
				e.printStackTrace();
			}
			// Repositionnement de la Map en session
			session.setAttribute(SESSION_ORDERS, orders);
		}

		response.sendRedirect(request.getContextPath() + "/listOrders");
	}

}
