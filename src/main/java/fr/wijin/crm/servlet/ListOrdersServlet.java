package fr.wijin.crm.servlet;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/listOrders")
public class ListOrdersServlet extends HttpServlet {

	private static final long serialVersionUID = 8351383211176431452L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Affichage de la liste des commandes
		this.getServletContext().getRequestDispatcher("/WEB-INF/listOrders.jsp").forward(request, response);
	}
}
