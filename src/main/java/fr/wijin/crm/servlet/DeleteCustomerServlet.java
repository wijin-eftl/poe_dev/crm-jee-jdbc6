package fr.wijin.crm.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import fr.wijin.crm.dao.CustomerDao;
import fr.wijin.crm.dao.impl.DaoFactory;
import fr.wijin.crm.exception.DaoException;
import fr.wijin.crm.form.FormUtils;
import fr.wijin.crm.model.Customer;

@WebServlet("/deleteCustomer")
public class DeleteCustomerServlet extends HttpServlet {

	private static final long serialVersionUID = -5804756307157436368L;

	public static final String SESSION_CUSTOMERS = "customers";
	
	private CustomerDao customerDao;
	
	@Override
	public void init() throws ServletException {
        this.customerDao = ((DaoFactory) getServletContext().getAttribute("daoFactory")).getCustomerDao();
    }

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String customerId = FormUtils.getParameterValue(request, "customerId");
		Integer id = Integer.parseInt(customerId);

		// Récupération de la Map des clients enregistrés en session 
		HttpSession session = request.getSession();
		Map<Integer, Customer> customers = (HashMap<Integer, Customer>) session.getAttribute(SESSION_CUSTOMERS);

		if (id != null && customers != null) {
			try {
				this.customerDao.deleteCustomer(customers.get(id));
				// Suppression du client de la Map
				customers.remove(id);
				session.removeAttribute("error");
			} catch (DaoException e) {
				e.printStackTrace();
				// Par exemple : afficher un message à l'utilisateur via un attribut
				session.setAttribute("error", "Impossible de supprimer l'utilisateur!");
			}
			// Repositionnement de la Map en session
			session.setAttribute(SESSION_CUSTOMERS, customers);
		}

		response.sendRedirect(request.getContextPath() + "/listCustomers");
	}

}
