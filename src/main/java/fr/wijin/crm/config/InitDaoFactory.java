package fr.wijin.crm.config;

import fr.wijin.crm.dao.impl.DaoFactory;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

@WebListener
public class InitDaoFactory implements ServletContextListener {
	
	private static final String ATT_DAO_FACTORY = "daoFactory";

	@Override
	public void contextInitialized(ServletContextEvent event) {
		//Récupération du ServletContext lors du chargement de l'application
		ServletContext servletContext = event.getServletContext();
		// Instanciation de la DaoFactory
		DaoFactory daoFactory = DaoFactory.getInstance();
		// Enregistrement dans un attribut ayant pour portée toute l'application
		servletContext.setAttribute(ATT_DAO_FACTORY, daoFactory);
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		// Rien à réaliser lors de la fermeture de l'application
	}
}