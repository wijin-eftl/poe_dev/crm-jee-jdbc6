package fr.wijin.crm.form;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import fr.wijin.crm.dao.CustomerDao;
import fr.wijin.crm.dao.OrderDao;
import fr.wijin.crm.exception.DaoException;
import fr.wijin.crm.exception.FormValidationException;
import fr.wijin.crm.model.Customer;
import fr.wijin.crm.model.Order;

public class CreateOrderForm implements Serializable {

	private static final long serialVersionUID = -4193073075066204599L;
	
	private static final String LABEL_FIELD = "label";
	private static final String ADRET_FIELD = "adrEt";
	private static final String NUMBER_OF_DAYS_FIELD = "numberOfDays";
	private static final String TVA_FIELD = "tva";
	private static final String STATUS_FIELD = "status";
	private static final String TYPE_FIELD = "type";
	private static final String NOTES_FIELD = "orderNotes";

	private String result;
	private Map<String, String> errors = new HashMap<>();
	private CustomerDao customerDao;
	private OrderDao orderDao;
	
	public CreateOrderForm(CustomerDao customerDao, OrderDao orderDao) {
		this.customerDao = customerDao;
		this.orderDao = orderDao;
	}

	public Map<String, String> getErrors() {
		return errors;
	}

	public String getResult() {
		return result;
	}

	public Order createOrder(HttpServletRequest request) {
		
		Customer customer;
		
        // Si client déjà existant, pas de validation à effectuer
        String choixNouveauClient = FormUtils.getParameterValue(request, "customerChoice");
        if ("oldCustomer".equals(choixNouveauClient)) {
        	// Récupération de l'identifiant du client choisi
            String oldCustomerId = FormUtils.getParameterValue(request, "customersList");
            Integer id = null;
            try {
                id = Integer.parseInt(oldCustomerId);
            } catch ( NumberFormatException e ) {
                setError("customerChoice", "Client inconnu, merci d'utiliser le formulaire prévu à cet effet." );
                id = 0;
            }
            
            //Récupération de l'objet Customer correspondant dans la session
            HttpSession session = request.getSession();
            customer = ((Map<Integer, Customer>) session.getAttribute("customers")).get(id);
        } else {
        	// Passage de la requête à la méthode createCustomer de CreateCustomerForm
        	CreateCustomerForm customerForm = new CreateCustomerForm(customerDao);
    		customer = customerForm.createCustomer(request);

    		// Récupération des éventuelles erreurs
            errors = customerForm.getErrors();
        }

		String label = FormUtils.getParameterValue(request, LABEL_FIELD);
		String adrEt = FormUtils.getParameterValue(request, ADRET_FIELD);
		String numberOfDays = FormUtils.getParameterValue(request, NUMBER_OF_DAYS_FIELD);
		String tva = FormUtils.getParameterValue(request, TVA_FIELD);
		String status = FormUtils.getParameterValue(request, STATUS_FIELD);
		String type = FormUtils.getParameterValue(request, TYPE_FIELD);
		String notes = FormUtils.getParameterValue(request, NOTES_FIELD);
		
		Order order = new Order();
		
		try {
			customerProcessing(customer, order);
			labelProcessing(label, order);
			adrEtProcessing(adrEt, order);
			numberOfDaysProcessing(numberOfDays, order);
			tvaProcessing(tva, order);
			
			order.setStatus(status);
			order.setType(type);
			order.setNotes(notes);
	
			if (errors.isEmpty()) {
				orderDao.createOrder(order);
				result = "Succès de la création de la commande.";
			} else {
				result = "Échec de la création de la commande.";
			}
		} catch (DaoException e) {
			setError( "imprévu", "Erreur imprévue lors de la création" );
            result = "Échec de la création de la commande : une erreur est survenue";
            e.printStackTrace();
		}
		return order;
	}
	
	
	private void customerProcessing(Customer customer, Order order) {
        if (customer == null) {
            setError("customerChoice", "Client inconnu, merci d'utiliser le formulaire" );
        }
        order.setCustomer(customer);
    }
	
	private void labelProcessing(String label, Order order) {
		try {
			labelValidation(label);
        } catch (FormValidationException e) {
            setError(LABEL_FIELD, e.getMessage());
        }
        order.setLabel(label);
    }
	
	private void labelValidation(String label) throws FormValidationException {
		if (label != null) {
			if (label.length() < 3) {
				throw new FormValidationException("Le libellé doit contenir au moins 3 caractères");
			}
		} else {
			throw new FormValidationException("Merci de saisir un libellé");
		}
	}
	
	private void adrEtProcessing(String adrEt, Order order) {
		double adrEtValue = -1;
		try {
			adrEtValue = doubleFieldValidation(adrEt);
		} catch (FormValidationException e) {
			setError(ADRET_FIELD, e.getMessage());
		}
		order.setAdrEt(adrEtValue);
	}
	
	private void numberOfDaysProcessing(String numberOfDays, Order order) {
		double numberOfDaysValue = -1;
		try {
			numberOfDaysValue = doubleFieldValidation(numberOfDays);
		} catch (FormValidationException e) {
			setError(NUMBER_OF_DAYS_FIELD, e.getMessage());
		}
		order.setNumberOfDays(numberOfDaysValue);
	}
	
	private void tvaProcessing(String tva, Order order) {
		double tvaValue = -1;
		try {
			tvaValue = doubleFieldValidation(tva);
		} catch (Exception e) {
			setError(TVA_FIELD, e.getMessage());
		}
		order.setTva(tvaValue);
	}
	
	/**
	 * Validation des champs de type double
	 * @param value la valeur au format "String"
	 * @return la valeur au format "double"
	 * @throws FormValidationException
	 */
	private double doubleFieldValidation(String value) throws FormValidationException {
		double doubleValue;
		if (value != null) {
			try {
				doubleValue = Double.parseDouble(value);
				if (doubleValue < 0) {
					throw new FormValidationException("Doit être un nombre positif");
				}
			} catch (NumberFormatException e) {
				throw new FormValidationException("Doit être un nombre");
			}
		} else {
			throw new FormValidationException("Champ obligatoire");
		}
		return doubleValue;
	}

	/**
	 * Positionnement d'un message d'erreur pour un paramètre
	 * @param parameter le nom du paramètre
	 * @param message le message d'erreur
	 */
	private void setError(String parameter, String message) {
		errors.put(parameter, message);
	}

}
