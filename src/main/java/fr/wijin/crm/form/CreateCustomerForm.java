package fr.wijin.crm.form;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import jakarta.servlet.http.HttpServletRequest;
import fr.wijin.crm.dao.CustomerDao;
import fr.wijin.crm.exception.DaoException;
import fr.wijin.crm.exception.FormValidationException;
import fr.wijin.crm.model.Customer;

public class CreateCustomerForm implements Serializable {

	private static final long serialVersionUID = 3937256929481737399L;
	
	private static final String LASTNAME_FIELD = "lastname";
	private static final String FIRSTNAME_FIELD = "firstname";
	private static final String COMPANY_FIELD = "company";
	private static final String MAIL_FIELD = "mail";
	private static final String PHONE_FIELD = "phone";
	private static final String MOBILE_FIELD = "mobile";
	private static final String NOTES_FIELD = "notes";

	private String result;
	private Map<String, String> errors = new HashMap<>();
	private CustomerDao customerDao;
	
	public CreateCustomerForm() {
		// Constructeur par défaut
    }
	
	public CreateCustomerForm(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

	public Map<String, String> getErrors() {
		return errors;
	}

	public String getResult() {
		return result;
	}

	public Customer createCustomer(HttpServletRequest request) {
		
		String lastname = FormUtils.getParameterValue(request, LASTNAME_FIELD);
		String firstname = FormUtils.getParameterValue(request, FIRSTNAME_FIELD);
		String company = FormUtils.getParameterValue(request, COMPANY_FIELD);
		String mail = FormUtils.getParameterValue(request, MAIL_FIELD);
		String phone = FormUtils.getParameterValue(request, PHONE_FIELD);
		String mobile = FormUtils.getParameterValue(request, MOBILE_FIELD);
		String notes = FormUtils.getParameterValue(request, NOTES_FIELD);
		
		Customer customer = new Customer();
		
		lastnameProcessing(lastname, customer);
		firstnameProcessing(firstname, customer);
		companyProcessing(company, customer);
		mailProcessing(mail, customer);
		phoneProcessing(phone, customer);

		customer.setMobile(mobile);
		customer.setNotes(notes);

		try {
			if (errors.isEmpty()) {
				this.customerDao.createCustomer(customer);
				this.result = "Succès de la création du client";
			} else {
				this.result = "Échec de la création du client";
			}
		} catch (DaoException e) {
			 setError( "imprévu", "Erreur imprévue lors de la création" );
	         this.result = "Échec de la création du client : " + e.getMessage();
	         e.printStackTrace();
		}

		return customer;
	}
	
	private void lastnameProcessing(String lastname, Customer customer) {
        try {
        	lastnameValidation(lastname);
        } catch (FormValidationException e) {
            setError(LASTNAME_FIELD, e.getMessage());
        }
        customer.setLastname(lastname);
    }

	private void lastnameValidation(String lastname) throws FormValidationException {
		if (lastname != null) {
			if (lastname.length() < 2) {
				throw new FormValidationException("Le nom doit contenir au moins 2 caractères");
			}
		} else {
			throw new FormValidationException("Merci de saisir un nom");
		}
	}
	
	private void firstnameProcessing(String firstname, Customer customer) {
        try {
        	firstnameValidation(firstname);
        } catch (FormValidationException e) {
            setError(FIRSTNAME_FIELD, e.getMessage());
        }
        customer.setFirstname(firstname);
    }

	private void firstnameValidation(String firstname) throws FormValidationException {
		if (firstname != null && firstname.length() < 2) {
			throw new FormValidationException("Le prénom doit contenir au moins 2 caractères");
		}
	}
	
	private void companyProcessing(String company, Customer customer) {
        try {
        	companyValidation(company);
        } catch (FormValidationException e) {
            setError(COMPANY_FIELD, e.getMessage());
        }
        customer.setCompany(company);
    }

	private void companyValidation(String company) throws FormValidationException {
		if (company != null) {
			if (company.length() < 3) {
				throw new FormValidationException("L'entreprise doit contenir au moins 3 caractères");
			}
		} else {
			throw new FormValidationException("Merci de saisir une entreprise");
		}
	}
	
	private void mailProcessing(String mail, Customer customer) {
		try {
			mailValidation(mail);
		} catch (FormValidationException e) {
			setError(MAIL_FIELD, e.getMessage());
		}
		customer.setMail(mail);
	}
	
	private void mailValidation(String mail) throws FormValidationException {
		if (mail != null) {
			if (!mail.matches("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)")) {
				throw new FormValidationException("Merci de saisir une adresse mail valide.");
			}
		} else {
			throw new FormValidationException("Merci de saisir une adresse mail.");
		}
	}
	
	private void phoneProcessing(String phone, Customer customer) {
		try {
			phoneValidation(phone);
		} catch (FormValidationException e) {
			setError(PHONE_FIELD, e.getMessage());
		}
		customer.setPhone(phone);
	}

	private void phoneValidation(String phone) throws FormValidationException {
		if (phone != null) {
			if (!phone.matches("^\\d+$")) {
				throw new FormValidationException("Le numéro de téléphone doit uniquement contenir des chiffres");
			} else if (phone.length() < 4) {
				throw new FormValidationException("Le numéro de téléphone doit contenir au moins 4 chiffres");
			}
		} else {
			throw new FormValidationException("Merci de saisir un numéro de téléphone");
		}
	}

	/**
	 * Positionnement d'un message d'erreur pour un paramètre
	 * @param parameter le nom du paramètre
	 * @param message le message d'erreur
	 */
	private void setError(String parameter, String message) {
		errors.put(parameter, message);
	}

}
