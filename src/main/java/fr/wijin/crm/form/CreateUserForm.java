package fr.wijin.crm.form;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import fr.wijin.crm.dao.UserDao;
import fr.wijin.crm.exception.DaoException;
import fr.wijin.crm.exception.FormValidationException;
import fr.wijin.crm.model.User;
import jakarta.servlet.http.HttpServletRequest;

public class CreateUserForm implements Serializable {

	private static final long serialVersionUID = 4952432887991549851L;
	
	private static final String USERNAME_FIELD = "username";
	private static final String PASSWORD_FIELD = "password";
	private static final String MAIL_FIELD = "mail";
	private static final String GRANTS_FIELD = "grants";

	private String result;
	private Map<String, String> errors = new HashMap<>();
	private UserDao userDao;
	
	public CreateUserForm() {
		// Constructeur par défaut
	}
	
	public CreateUserForm(UserDao userDao) {
		this.userDao = userDao;
	}

	public Map<String, String> getErrors() {
		return errors;
	}

	public String getResult() {
		return result;
	}

	public User createUser(HttpServletRequest request) {
		String username = FormUtils.getParameterValue(request, USERNAME_FIELD);
		String password = FormUtils.getParameterValue(request, PASSWORD_FIELD);
		String mail = FormUtils.getParameterValue(request, MAIL_FIELD);
		String grants = FormUtils.getParameterValue(request, GRANTS_FIELD);

		User user = new User();

		try {
			usernameValidation(username);
		} catch (Exception e) {
			setError(USERNAME_FIELD, e.getMessage());
		}
		user.setUsername(username);

		try {
			passwordValidation(password);
		} catch (Exception e) {
			setError(PASSWORD_FIELD, e.getMessage());
		}
		user.setPassword(password);;

		try {
			mailValidation(mail);
		} catch (Exception e) {
			setError(MAIL_FIELD, e.getMessage());
		}
		user.setMail(mail);

		try {
			grantsValidation(grants);
		} catch (Exception e) {
			setError(GRANTS_FIELD, e.getMessage());
		}
		user.setGrants(grants);

		try {
			if (errors.isEmpty()) {
				this.userDao.createUser(user);
				this.result = "Succès de la création de l'utilisateur.";
			} else {
				this.result = "Échec de la création de l'utilisateur.";
			}
		} catch (DaoException e) {
			 setError( "imprévu", "Erreur imprévue lors de la création" );
	         this.result = "Échec de la création de l'utilisateur : " + e.getMessage();
	         e.printStackTrace();
		}

		return user;
	}

	private void usernameValidation(String username) throws FormValidationException {
		if (username != null) {
			if (username.length() < 2) {
				throw new FormValidationException("Le username doit contenir au moins 2 caractères.");
			}
		} else {
			throw new FormValidationException("Merci de saisir un username.");
		}
	}

	private void passwordValidation(String password) throws FormValidationException {
		if (password != null) {
			if (password.length() < 8) {
				throw new FormValidationException("Le password doit contenir au moins 8 caractères.");
			}
		} else {
			throw new FormValidationException("Merci de saisir un password.");
		}
	}

	private void mailValidation(String mail) throws FormValidationException {
		if (mail != null) {
			if (!mail.matches("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)")) {
				throw new FormValidationException("Merci de saisir une adresse mail valide.");
			}
		} else {
			throw new FormValidationException("Merci de saisir une adresse mail.");
		}
	}
	
	private void grantsValidation(String grants) throws FormValidationException {
		if (grants != null) {
			if (!"ADMIN".equals(grants) && !"USER".equals(grants)) {
				throw new FormValidationException("Les droits doivent être ADMIN ou USER.");
			}
		} else {
			throw new FormValidationException("Merci de saisir un niveau de droits.");
		}
	}

	/**
	 * Positionnement d'un message d'erreur pour un paramètre
	 * @param parameter le nom du paramètre
	 * @param message le message d'erreur
	 */
	private void setError(String parameter, String message) {
		errors.put(parameter, message);
	}

}
