package fr.wijin.crm.dao;

import java.util.List;

import fr.wijin.crm.exception.DaoException;
import fr.wijin.crm.model.Order;

public interface OrderDao {

	/**
	 * Get an order by id 
	 * @param id the order id
	 * @return the order
	 * @throws DaoException
	 */
	Order getById(Integer id) throws DaoException;

	/**
	 * Get all orders
	 * @return a list of orders
	 * @throws DaoException
	 */
	List<Order> getAllOrders() throws DaoException;

	/**
	 * Create an order
	 * @param order the order to create
	 * @throws DaoException
	 */
	void createOrder(Order order) throws DaoException;
	
	/**
	 * Update an order
	 * @param order the order to update
	 * @throws DaoException
	 */
	void updateOrder(Order order) throws DaoException;

	/**
	 * Delete an order
	 * @param order the order to delete
	 * @throws DaoException
	 */
	void deleteOrder(Order order) throws DaoException;

}
