package fr.wijin.crm.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.wijin.crm.dao.CustomerDao;
import fr.wijin.crm.dao.OrderDao;
import fr.wijin.crm.exception.DaoException;
import fr.wijin.crm.model.Order;

public class OrderDaoImpl implements OrderDao {

	private static final String SQL_SELECT = "SELECT id, customer_id, label, number_of_days, adr_et, tva, type, status, notes FROM orders ORDER BY id";
	private static final String SQL_SELECT_BY_ID = "SELECT id, customer_id, label, number_of_days, adr_et, tva, type, status, notes FROM orders WHERE id = ?";
	private static final String SQL_INSERT = "INSERT INTO orders (customer_id, label, number_of_days, adr_et, tva, type, status, notes) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String SQL_UPDATE_BY_ID = "UPDATE orders set customer_id = ?, label = ?, number_of_days = ?, adr_et = ?, tva = ?, type = ?, status = ?, notes = ? WHERE id = ?";
	private static final String SQL_DELETE_BY_ID = "DELETE FROM orders WHERE id = ?";

	private DaoFactory daoFactory;

	/**
	 * Constructor
	 * @param daoFactory
	 */
	OrderDaoImpl(DaoFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	/**
	 *  {@inheritDoc}
	 */
	@Override
	public Order getById(Integer id) throws DaoException {
		return find(SQL_SELECT_BY_ID, id);
	}

	/**
	 * Return an order by building a request with parameters
	 */
	private Order find(String sql, Object... objets) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Order order = null;
		try {
			connection = daoFactory.getConnection();
			
			preparedStatement = DaoUtils.initializePreparedStatement(connection, sql, false, objets);
			resultSet = preparedStatement.executeQuery();
		
			if (resultSet.next()) {
				order = map(resultSet);
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(resultSet, preparedStatement, connection);
		}
		return order;
	}

	/**
	 *  {@inheritDoc}
	 */
	@Override
	public List<Order> getAllOrders() throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Order> orders = new ArrayList<>();
		try {
			connection = daoFactory.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				orders.add(map(resultSet));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(resultSet, preparedStatement, connection);
		}
		return orders;
	}

	/**
	 *  {@inheritDoc}
	 */
	@Override
	public void createOrder(Order order) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet generatedValues = null;
		try {
			connection = daoFactory.getConnection();
			preparedStatement = DaoUtils.initializePreparedStatement(connection, SQL_INSERT, true,
					order.getCustomer().getId(), order.getLabel(), order.getNumberOfDays(), order.getAdrEt(),
					order.getTva(), order.getType(), order.getStatus(), order.getNotes());

			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new DaoException("Echec de la création de la commande, aucune ligne ajoutée dans la table.");
			}
			generatedValues = preparedStatement.getGeneratedKeys();
			if (generatedValues.next()) {
				order.setId(generatedValues.getInt(1));
			} else {
				throw new DaoException("Echec de la création de la commande en base, aucun ID auto-généré retourné.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(generatedValues, preparedStatement, connection);
		}
	}

	/**
	 *  {@inheritDoc}
	 */
	@Override
	public void updateOrder(Order order) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = daoFactory.getConnection();
			preparedStatement = DaoUtils.initializePreparedStatement(connection, SQL_UPDATE_BY_ID, true,
					order.getCustomer().getId(), order.getLabel(), order.getNumberOfDays(), order.getAdrEt(),
					order.getTva(), order.getType(), order.getStatus(), order.getNotes(), order.getId());
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new DaoException("Echec de la modification de la commande.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(preparedStatement, connection);
		}
	}

	/**
	 *  {@inheritDoc}
	 */
	@Override
	public void deleteOrder(Order order) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = daoFactory.getConnection();
			preparedStatement = DaoUtils.initializePreparedStatement(connection, SQL_DELETE_BY_ID, true,
					order.getId());
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new DaoException("Echec de la suppression de la commande, aucune ligne supprimée de la table.");
			} else {
				order.setId(null);
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(preparedStatement, connection);
		}
	}

	/**
	 * Mapping between ResultSet and bean Order
	 */
	private Order map(ResultSet resultSet) throws SQLException {
		Order order = new Order();

		// Get customer id by calling customerDao
		CustomerDao customerDao = daoFactory.getCustomerDao();
		order.setCustomer(customerDao.getCustomerById(resultSet.getInt("customer_id")));
		order.setId(resultSet.getInt("id"));
		order.setLabel(resultSet.getString("label"));
		order.setNumberOfDays(resultSet.getDouble("number_of_days"));
		order.setAdrEt(resultSet.getDouble("adr_et"));
		order.setTva(resultSet.getDouble("tva"));
		order.setType(resultSet.getString("type"));
		order.setStatus(resultSet.getString("status"));
		order.setNotes(resultSet.getString("notes"));
		return order;
	}
}
