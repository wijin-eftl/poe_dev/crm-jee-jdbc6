package fr.wijin.crm.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.wijin.crm.dao.UserDao;
import fr.wijin.crm.exception.DaoException;
import fr.wijin.crm.model.User;

public class UserDaoImpl implements UserDao {

	private static final String SQL_SELECT_BY_ID = "SELECT id, username, password, mail, grants FROM users WHERE id = ?";
	
	private static final String  SQL_SELECT_BY_USERNAME = "SELECT id, username, password, mail, grants FROM users WHERE username = ?";
	
	private static final String SQL_SELECT_ALL = "SELECT id, username, password, mail, grants FROM users";
	
	private static final String SQL_INSERT = "INSERT INTO users (username, password, mail, grants) VALUES (?, ?, ?, ?)";
	
	private static final String SQL_UPDATE_BY_ID = "UPDATE users SET username = ?, password = ?, mail = ?, grants = ? WHERE id = ?";
	
	private static final String SQL_DELETE_BY_ID = "DELETE FROM users WHERE id = ?";

	private DaoFactory daoFactory;

	/**
	 * Constructor
	 * 
	 * @param daoFactory
	 */
	UserDaoImpl(DaoFactory daoFactory) {
		this.daoFactory = daoFactory;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public User getById(Integer id) throws DaoException {
		return find(SQL_SELECT_BY_ID, id);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public User getByUsername(String username) throws DaoException {
		return find(SQL_SELECT_BY_USERNAME, username);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<User> getAll() throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<User> users = new ArrayList<>();
		try {
			connection = daoFactory.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_ALL);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				users.add(map(resultSet));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(resultSet, preparedStatement, connection);
		}
		return users;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createUser(User user) throws DaoException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet generatedValues = null;
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = DaoUtils.initializePreparedStatement(connexion, SQL_INSERT, true,
					user.getUsername(), user.getPassword(), user.getMail(), user.getGrants());
			int statut = preparedStatement.executeUpdate();

			// Check status
			if (statut == 0) {
				throw new DaoException("Echec de la création du user, aucune ligne ajoutée dans la table.");
			}

			// Get generated key
			generatedValues = preparedStatement.getGeneratedKeys();
			if (generatedValues.next()) {
				// Initialize id value with generated key
				user.setId(generatedValues.getInt(1));
			} else {
				throw new DaoException("Echec de la création du user en base, aucun ID auto-généré retourné.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(generatedValues, preparedStatement, connexion);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateUser(User user) throws DaoException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = DaoUtils.initializePreparedStatement(connexion, SQL_UPDATE_BY_ID, false,
					user.getUsername(), user.getPassword(), user.getMail(), user.getGrants());
			int statut = preparedStatement.executeUpdate();

			// Check status
			if (statut == 0) {
				throw new DaoException("Echec de la modification du user.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(preparedStatement, connexion);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteUser(User user) throws DaoException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = DaoUtils.initializePreparedStatement(connexion, SQL_DELETE_BY_ID, false,
					user.getId());
			int statut = preparedStatement.executeUpdate();

			// Check status
			if (statut == 0) {
				throw new DaoException("Echec de la suppression du user.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(preparedStatement, connexion);
		}
	}
	
	/**
	 * Return a user by building a request with parameters
	 * 
	 * @return User
	 */
	private User find(String sql, Object... objets) throws DaoException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		User user = null;
		try {
			connexion = daoFactory.getConnection();

			preparedStatement = DaoUtils.initializePreparedStatement(connexion, sql, false, objets);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				user = map(resultSet);
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(resultSet, preparedStatement, connexion);
		}
		return user;
	}
	
	/**
	 * Mapping between ResultSet and bean User
	 */
	private static User map(ResultSet resultSet) throws SQLException {
		User user = new User();
		user.setId(resultSet.getInt("id"));
		user.setUsername(resultSet.getString("username"));
		user.setPassword(resultSet.getString("password"));
		user.setMail(resultSet.getString("mail"));
		user.setGrants(resultSet.getString("grants"));
		return user;
	}
}
