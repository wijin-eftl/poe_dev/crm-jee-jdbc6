package fr.wijin.crm.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.wijin.crm.dao.CustomerDao;
import fr.wijin.crm.exception.DaoException;
import fr.wijin.crm.model.Customer;

public class CustomerDaoImpl implements CustomerDao {

	private static final String SQL_SELECT_ALL = "SELECT id, firstname, lastname, company, mail, phone, mobile, notes, active FROM customers";

	private static final String SQL_SELECT_BY_ID = "SELECT id, firstname, lastname, company, mail, phone, mobile, notes, active FROM customers WHERE id = ?";

	private static final String SQL_SELECT_BY_LASTNAME = "SELECT id, firstname, lastname, company, mail, phone, mobile, notes, active FROM customers WHERE lastname = ?";

	private static final String SQL_INSERT = "INSERT INTO customers (firstname, lastname, company, mail, phone, mobile, notes, active) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String SQL_UPDATE_BY_ID = "UPDATE customers set firstname = ?, lastname = ?, company = ?, mail = ?, phone = ?, mobile = ?, notes = ?, active = ? WHERE id = ?";

	private static final String SQL_DELETE_BY_ID = "DELETE FROM customers WHERE id = ?";

	private DaoFactory daoFactory;

	/**
	 * Constructor
	 * 
	 * @param daoFactory
	 */
	CustomerDaoImpl(DaoFactory daoFactory) {
		this.daoFactory = daoFactory;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Customer> getAllCustomers() throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Customer> customers = new ArrayList<>();
		try {
			connection = daoFactory.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_ALL);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				customers.add(map(resultSet));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(resultSet, preparedStatement, connection);
		}
		return customers;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Customer getCustomerByLastname(String lastname) throws DaoException {
		return find(SQL_SELECT_BY_LASTNAME, lastname);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Customer getCustomerById(Integer id) throws DaoException {
		return find(SQL_SELECT_BY_ID, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createCustomer(Customer customer) throws DaoException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet generatedValues = null;
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = DaoUtils.initializePreparedStatement(connexion, SQL_INSERT, true,
					customer.getFirstname(), customer.getLastname(), customer.getCompany(), customer.getMail(),
					customer.getPhone(), customer.getMobile(), customer.getNotes(), customer.isActive());
			int statut = preparedStatement.executeUpdate();

			// Check status
			if (statut == 0) {
				throw new DaoException("Echec de la création du client, aucune ligne ajoutée dans la table.");
			}

			// Get generated key
			generatedValues = preparedStatement.getGeneratedKeys();
			if (generatedValues.next()) {
				// Initialize id value with generated key
				customer.setId(generatedValues.getInt(1));
			} else {
				throw new DaoException("Echec de la création du client en base, aucun ID auto-généré retourné.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(generatedValues, preparedStatement, connexion);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateCustomer(Customer customer) throws DaoException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		System.out.println("Customer to update = " + customer.toString());
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = DaoUtils.initializePreparedStatement(connexion, SQL_UPDATE_BY_ID, false,
					customer.getFirstname(), customer.getLastname(), customer.getCompany(), customer.getMail(),
					customer.getPhone(), customer.getMobile(), customer.getNotes(), customer.isActive(), customer.getId());
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new DaoException("Echec de la modification du client.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(preparedStatement, connexion);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteCustomer(Customer customer) throws DaoException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = DaoUtils.initializePreparedStatement(connexion, SQL_DELETE_BY_ID, false,
					customer.getId());
			int statut = preparedStatement.executeUpdate();
			if (statut == 0) {
				throw new DaoException("Echec de la suppression du client, aucune ligne supprimée de la table.");
			} else {
				customer.setId(null);
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(preparedStatement, connexion);
		}
	}

	/**
	 * Return a customer by building a request with parameters
	 * 
	 * @return Customer
	 */
	private Customer find(String sql, Object... objets) throws DaoException {
		Connection connexion = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Customer client = null;
		try {
			connexion = daoFactory.getConnection();

			preparedStatement = DaoUtils.initializePreparedStatement(connexion, sql, false, objets);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				client = map(resultSet);
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DaoUtils.close(resultSet, preparedStatement, connexion);
		}
		return client;
	}

	/**
	 * Mapping between ResultSet and bean Customer
	 */
	private static Customer map(ResultSet resultSet) throws SQLException {
		Customer customer = new Customer();
		customer.setId(resultSet.getInt("id"));
		customer.setFirstname(resultSet.getString("firstname"));
		customer.setLastname(resultSet.getString("lastname"));
		customer.setCompany(resultSet.getString("company"));
		customer.setMail(resultSet.getString("mail"));
		customer.setPhone(resultSet.getString("phone"));
		customer.setMobile(resultSet.getString("mobile"));
		customer.setNotes(resultSet.getString("notes"));
		customer.setActive(resultSet.getBoolean("active"));
		return customer;
	}

}
