package fr.wijin.crm.dao.impl;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

import fr.wijin.crm.dao.CustomerDao;
import fr.wijin.crm.dao.OrderDao;
import fr.wijin.crm.dao.UserDao;
import fr.wijin.crm.exception.DaoConfigurationException;

public class DaoFactory {

	private static final String PROPERTIES_FILE = "fr/wijin/crm/dao/dao.properties";
	private static final String PROPERTY_URL = "url";
	private static final String PROPERTY_DRIVER = "driver";
	private static final String PROPERTY_USERNAME = "username";
	private static final String PROPERTY_PASSWORD = "password";
	
	private DataSource dataSource;
	
	private DaoFactory(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Récupération de l'instance Factory
	 * 
	 * @return DaoFactory
	 */
	public static DaoFactory getInstance() throws DaoConfigurationException {
		Properties properties = new Properties();
		String url;
		String driver;
		String username;
		String password;

		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream fichierProperties = classLoader.getResourceAsStream(PROPERTIES_FILE);

		if (fichierProperties == null) {
			throw new DaoConfigurationException("Le fichier properties " + PROPERTIES_FILE + " est introuvable.");
		}

		try {
			properties.load(fichierProperties);
			url = properties.getProperty(PROPERTY_URL);
			driver = properties.getProperty(PROPERTY_DRIVER);
			username = properties.getProperty(PROPERTY_USERNAME);
			password = properties.getProperty(PROPERTY_PASSWORD);
		} catch (IOException e) {
			throw new DaoConfigurationException("Impossible de charger le fichier properties " + PROPERTIES_FILE, e);
		}

		BasicDataSource basicDs = new BasicDataSource();
		basicDs.setDriverClassName(driver);
		basicDs.setUrl(url);
		basicDs.setUsername(username);
		basicDs.setPassword(password);
		// Parameters for connection pooling
		basicDs.setInitialSize(10);
		basicDs.setMaxTotal(10);

		return new DaoFactory(basicDs);
	}

	/**
	 * Récupération de la connexion à la BDD
	 * 
	 * @return Connection
	 */
	public Connection getConnection() throws SQLException {
		return this.dataSource.getConnection();
	}

	/**
	 * Récupération du CustomerDao
	 * 
	 * @return CustomerDao
	 */
	public CustomerDao getCustomerDao() {
		return new CustomerDaoImpl(this);
	}

	/**
	 * Récupération du OrderDao
	 * 
	 * @return OrderDao
	 */
	public OrderDao getOrderDao() {
		return new OrderDaoImpl(this);
	}
	
	/**
	 * Récupération du UserDao
	 * 
	 * @return UserDao
	 */
	public UserDao getUserDao() {
		return new UserDaoImpl(this);
	}

}
