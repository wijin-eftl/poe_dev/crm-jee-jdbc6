package fr.wijin.crm.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.wijin.crm.dao.CustomerDao;
import fr.wijin.crm.dao.OrderDao;
import fr.wijin.crm.dao.UserDao;
import fr.wijin.crm.dao.impl.DaoFactory;
import fr.wijin.crm.model.Customer;
import fr.wijin.crm.model.Order;
import fr.wijin.crm.model.User;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

@WebFilter("/*")
public class LoadingFilter implements Filter {

	public static final String ATT_DAO_FACTORY = "daoFactory";
	public static final String SESSION_CUSTOMERS = "customers";
	public static final String SESSION_ORDERS = "orders";
	public static final String SESSION_USERS = "users";

	private CustomerDao customerDao;
	private OrderDao orderDao;
	private UserDao userDao;

	@Override
	public void init(FilterConfig config) throws ServletException {
		// Récupération des instances Dao
		DaoFactory daoFactory = (DaoFactory) config.getServletContext().getAttribute(ATT_DAO_FACTORY);
		this.customerDao = daoFactory.getCustomerDao();
		this.orderDao = daoFactory.getOrderDao();
		this.userDao = daoFactory.getUserDao();
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		// Cast
		HttpServletRequest request = (HttpServletRequest) req;

		// Récupération de la session depuis la requête
		HttpSession session = request.getSession();

		/*
		 * Si la Map des clients n'existe pas en session, alors l'utilisateur se
		 * connecte pour la première fois et il faut charger en session les informations
		 * contenues dans la BDD
		 */
		if (session.getAttribute(SESSION_CUSTOMERS) == null) {
			// Récupération de la liste des clients existants et enregistrement en session
			List<Customer> customersList = customerDao.getAllCustomers();
			Map<Integer, Customer> clientsMap = new HashMap<>();
			for (Customer customer : customersList) {
				clientsMap.put(customer.getId(), customer);
			}
			session.setAttribute(SESSION_CUSTOMERS, clientsMap);
		}

		// Commandes
		if (session.getAttribute(SESSION_ORDERS) == null) {
			// Récupération de la liste des commandes existantes et enregistrement en
			// session
			List<Order> ordersList = orderDao.getAllOrders();
			Map<Integer, Order> ordersMap = new HashMap<>();
			for (Order order : ordersList) {
				ordersMap.put(order.getId(), order);
			}
			session.setAttribute(SESSION_ORDERS, ordersMap);
		}

		// Utilisateurs
		if (session.getAttribute(SESSION_USERS) == null) {
			// Récupération de la liste des utilisateurs existants et enregistrement en
			// session
			List<User> usersList = userDao.getAll();
			Map<Integer, User> usersMap = new HashMap<>();
			for (User user : usersList) {
				usersMap.put(user.getId(), user);
			}
			session.setAttribute(SESSION_USERS, usersMap);
		}

		// Poursuite de la requête en cours
		chain.doFilter(request, res);
	}

	/*@Override
	public void destroy() {
		// Rien à faire
	}*/
}